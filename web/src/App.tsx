// import { Button } from 'antd';
import * as React from 'react';
import './App.css';

// import JSXLearn from './components/JSXLearn/JSXLearn'
// import PropsLearn from './components/PropsLearn/PropsLearn'
// import StateLearn from './components/StateLearn/StateLearn'
// import AxiosLearn from './components/AxiosLearn/AxiosLearn'
import RxjsLearn from './components/RxjsLearn/RxjsLearn'

import { IProps } from "./components/PropsLearn/type"

// 测试自己编写的简易 redux
// import './selfRedux/redux-test'

class App extends React.Component {

  toChild: IProps = {
    info: "",
    event: (info: string): void => {
      console.log("father receieved from child", info)
    }
  }

  render() {
    return (
      <div className="App">
        {/* <JSXLearn /> */}
        {/* <PropsLearn {...this.toChild} /> */}
        {/* <StateLearn /> */}
        {/* <AxiosLearn /> */}
        <RxjsLearn />
      </div>
    );
  }
}

export default App;
